package io.gitlab.jfronny.dynres.mixin;

import io.gitlab.jfronny.dynres.DynRes;
import net.minecraft.client.network.ClientCommonNetworkHandler;
import net.minecraft.network.packet.c2s.common.ResourcePackStatusC2SPacket;
import net.minecraft.network.packet.s2c.common.ResourcePackSendS2CPacket;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.net.URL;

@Mixin(ClientCommonNetworkHandler.class)
public abstract class ClientCommonNetworkHandlerMixin {
    @Shadow @Nullable protected static URL getParsedResourcePackUrl(String url) {
        throw new RuntimeException("Mixin not applied");
    }

    @Inject(at = @At("HEAD"), method = "onResourcePackSend(Lnet/minecraft/network/packet/s2c/common/ResourcePackSendS2CPacket;)V")
    public void logResourcePackStatus(ResourcePackSendS2CPacket status, CallbackInfo info) {
        DynRes.LOGGER.info("[RECEIVE] ResourcePackUrl={0}", status.url());
        DynRes.LOGGER.info("[RECEIVE] ResourcePackHash={0}", status.hash());
    }

    @Redirect(method = "onResourcePackSend(Lnet/minecraft/network/packet/s2c/common/ResourcePackSendS2CPacket;)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/network/ClientCommonNetworkHandler;getParsedResourcePackUrl(Ljava/lang/String;)Ljava/net/URL;"))
    public URL logResourcePackStatus(String url) {
        URL res = getParsedResourcePackUrl(url);
        DynRes.LOGGER.info("[CHECK] ResourcePackUrlValid={0}", res != null);
        return res;
    }
}
