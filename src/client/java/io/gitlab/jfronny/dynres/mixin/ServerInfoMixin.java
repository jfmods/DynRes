package io.gitlab.jfronny.dynres.mixin;

import io.gitlab.jfronny.dynres.DynRes;
import net.minecraft.client.network.ServerInfo;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ServerInfo.class)
public class ServerInfoMixin {
    @Inject(at = @At("RETURN"), method = "getResourcePackPolicy()Lnet/minecraft/client/network/ServerInfo$ResourcePackPolicy;")
    public void getResPackState(CallbackInfoReturnable<ServerInfo.ResourcePackPolicy> info) {
        DynRes.LOGGER.info("[RECEIVE] ResourcePackStatus={0}", info.getReturnValue());
    }
}
