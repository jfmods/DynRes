package io.gitlab.jfronny.dynres.mixin;

import io.gitlab.jfronny.dynres.Cfg;
import io.gitlab.jfronny.dynres.DynRes;
import io.gitlab.jfronny.libjf.web.api.v1.WebServer;
import io.gitlab.jfronny.libjf.web.impl.util.WebPaths;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.dedicated.ServerPropertiesHandler;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.UUID;

@Mixin(ServerPropertiesHandler.class)
public abstract class ServerPropertiesHandlerMixin {
    @Shadow @Nullable
    private static Text parseResourcePackPrompt(String prompt) {
        throw new IllegalStateException("Mixin not applied");
    }

    @Inject(method = "getServerResourcePackProperties(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Ljava/util/Optional;", at = @At("HEAD"), cancellable = true)
    private static void getServerResourcePackProperties(String id, String url, String sha1, String hash, boolean required, String prompt, CallbackInfoReturnable<Optional<MinecraftServer.ServerResourcePackProperties>> cir) {
        if (DynRes.packFile != null) {
            sha1 = "";
            if (Cfg.hashResources) {
                try {
                    StringBuilder result = new StringBuilder();
                    for (byte b : MessageDigest.getInstance("SHA-1").digest(Files.readAllBytes(DynRes.packFile))) {
                        result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
                    }
                    sha1 = result.toString();
                    DynRes.LOGGER.info("Set hash to {0}", sha1);
                } catch (IOException | NoSuchAlgorithmException e) {
                    DynRes.LOGGER.error("Failed to get hash, continuing with empty", e);
                }
            }
            url = WebPaths.concat(WebServer.getInstance().getServerRoot(), "resources.zip");
            cir.setReturnValue(Optional.of(new MinecraftServer.ServerResourcePackProperties(
                    UUID.nameUUIDFromBytes(url.getBytes(StandardCharsets.UTF_8)),
                    url,
                    sha1,
                    required,
                    parseResourcePackPrompt(prompt)
            )));
        }
    }
}
