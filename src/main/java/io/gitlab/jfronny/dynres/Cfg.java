package io.gitlab.jfronny.dynres;

import io.gitlab.jfronny.libjf.config.api.v2.Entry;
import io.gitlab.jfronny.libjf.config.api.v2.JfConfig;

@JfConfig(referencedConfigs = "libjf-web-v0")
public class Cfg  {
    @Entry public static String resourcesFile = "resources.zip";
    @Entry public static boolean hashResources = false;

    static {
        JFC_Cfg.ensureInitialized();
    }
}
