package io.gitlab.jfronny.dynres;

import net.fabricmc.api.ClientModInitializer;

public class DebugClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        DynRes.LOGGER.info("[DynRes DebugClient] This mod is not needed on clients. All it does here is log additional info");
    }
}
