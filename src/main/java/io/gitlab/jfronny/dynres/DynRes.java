package io.gitlab.jfronny.dynres;

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;
import io.gitlab.jfronny.libjf.web.api.v1.*;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DynRes implements WebEntrypoint {
    public static final SystemLoggerPlus LOGGER = SystemLoggerPlus.forName("DynRes");
    public static Path packFile;
    public static String packUrl = "";

    static {
        packFile = FabricLoader.getInstance().getGameDir().resolve(Cfg.resourcesFile);
        if (!Files.exists(packFile)) {
            if (FabricLoader.getInstance().getEnvironmentType() == EnvType.SERVER) {
                LOGGER.error("The resource file specified in your config could not be found. YOU MUST SPECIFY A RESOURCE PACK FOR IT TO BE SERVED!");
            }
            packFile = null;
        }
    }

    @Override
    public void register(WebServer api) {
        if (FabricLoader.getInstance().getEnvironmentType() == EnvType.SERVER && packFile != null) {
            try {
                packUrl = api.registerFile(PathSegment.of("resources.zip"), packFile, !Cfg.hashResources);
                LOGGER.info("Initialized DynRes at {0}", packUrl);
            } catch (IOException e) {
                LOGGER.error("Could not register DynRes resource pack", e);
            }
        }
    }
}
