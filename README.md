DynRes uses an integrated web server to serve a resource pack directly from the server directory.
It also modifies the server to specify that pack as the default (overwrites resource-pack and resource-pack-sha1 in server.properties).
You can configure the port for the web server, the file to host and whether to provide a hash in the config.

To use this mod on your server you need to place your resource pack in the configured place (default: server/resources.zip)
and change baseLink in the config to your public address (what players will enter)

This mod is only required on your server. If added to a client the only thing it will do is log some information about server connections